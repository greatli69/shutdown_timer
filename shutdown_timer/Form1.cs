﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace shutdown_timer
{
    public partial class Form1 : Form
    {
        private DateTime dt;

        // This delegate enables asynchronous calls for setting
        // the text property on a TextBox control.
        delegate void SetTextCallback(string text);

        public Form1()
        {
            InitializeComponent();
        }

        // This method demonstrates a pattern for making thread-safe
        // calls on a Windows Forms control. 
        //
        // If the calling thread is different from the thread that
        // created the TextBox control, this method creates a
        // SetTextCallback and calls itself asynchronously using the
        // Invoke method.
        //
        // If the calling thread is the same as the thread that created
        // the TextBox control, the Text property is set directly. 

        private void SetText(string text)
        {
            try
            {
                // InvokeRequired required compares the thread ID of the
                // calling thread to the thread ID of the creating thread.
                // If these threads are different, it returns true.
                if (this.lblCurTime.InvokeRequired)
                {
                    SetTextCallback d = new SetTextCallback(SetText);
                    this.Invoke(d, new object[] { text });
                }
                else
                {
                    this.lblCurTime.Text = text;
                }
            }
            catch (ObjectDisposedException ex)
            {

                
            }


        }

        public void theout(object source, System.Timers.ElapsedEventArgs e)
        {
            dt = DateTime.Now;

            this.SetText(dt.ToString());

            

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            System.Timers.Timer t = new System.Timers.Timer(1000);
            t.Elapsed += new System.Timers.ElapsedEventHandler(theout);
            t.AutoReset = true;
            t.Enabled = true;

            dt = DateTime.Now;
            lblCurTime.Text = dt.ToString();
        }
    }
}
